<?php

/**
 * Class to manager QueueManager
 *
 * @see Q-Manager
 *
 * @category  Snep
 * @package   Q-Manager
 * @copyright Copyright (c) 2016 Opens Tecnologia
 * @author    Tiago Zimmermann <tiago.zimmermann@opens.com.br>
 *
 */
class QueueManager_Manager {

    /**
     * Method to get all queues
     * @return <array>
     */
    public static function getQueueAll() {

        $db = Zend_registry::get('db');

        $select = $db->select()
                ->from("queues")
                ->order("name");

        $stmt = $db->query($select);
        $queues = $stmt->fetchAll();

        return $queues;
    }

    /**
     * Method to get all queues with permission user
     * @return <array>
     */
    public static function getQueueAllPermission($id) {

        $db = Zend_registry::get('db');

        $select = $db->select()
                ->from('users_queues_permissions')
                ->from('queues')
                ->where('users_queues_permissions.queue_id = queues.id')
                ->where("users_queues_permissions.user_id = ?", $id);

        $stmt = $db->query($select);
        $queues = $stmt->fetchAll();

        return $queues;
    }

    /**
     * Method to get queue by id
     * @param <string> $id
     * @return <array> $queue
     */
    public static function getQueue($id) {

        $db = Zend_registry::get('db');

        $select = $db->select()
                ->from("queues")
                ->where("queues.id = ?",$id);

        $stmt = $db->query($select);
        $queue = $stmt->fetch();

        return $queue;
    }

    /**
     * Method to get all group queues
     * @return <array>
     */
    public static function getGroupQueueAll() {

        $db = Zend_registry::get('db');

        $select = $db->select()
                ->from("group_queues");

        $stmt = $db->query($select);
        $groups = $stmt->fetchAll();

        return $groups;
    }

    /**
     * getBond - Method to get user bond
     * @param <int> $id
     * @return <array> $peers
     */
    public static function getBond($id) {

        $db = Zend_Registry::get("db");

        $select = $db->select()
                ->from('core_binds')
                ->joinLeft(array('p'=>'peers'),'core_binds.peer_name = p.name', 'callerid')
                ->where("core_binds.user_id = ?", $id);

        $stmt = $db->query($select);
        $peers = $stmt->fetchAll();

        return $peers;
    }

}

?>
