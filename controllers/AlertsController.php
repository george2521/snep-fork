<?php

/**
 * Q-Manager Agents Controller
 *
 * @category  Snep
 * @package   Q-Manager
 * @copyright Copyright (c) 2016 OpenS Tecnologia
 * @author    Tiago Zimmermann <tiago.zimmermann@opens.com.br>
 */
class SnepQManager_AlertsController extends Zend_Controller_Action {

    /**
     * Initial settings of the class
     */
     public function init() {

        $this->view->url = $this->getFrontController()->getBaseUrl() . '/' . $this->getRequest()->getModuleName(). '/' . $this->getRequest()->getControllerName();
        $this->view->lineNumber = Zend_Registry::get('config')->ambiente->linelimit;

        $this->connector = "http://127.0.0.1:3000/config/queue_alerts";

        $this->view->baseUrl = Zend_Controller_Front::getInstance()->getBaseUrl();
        $this->view->key = Snep_Dashboard_Manager::getKey(
            Zend_Controller_Front::getInstance()->getRequest()->getModuleName(),
            Zend_Controller_Front::getInstance()->getRequest()->getControllerName(),
            Zend_Controller_Front::getInstance()->getRequest()->getActionName());
    }

    /**
     * List all Alerts
     */
    public function indexAction() {

        $this->view->breadcrumb = Snep_Breadcrumb::renderPath(array(
                    $this->view->translate("Q-Manager"),
                    $this->view->translate("Configuração de Alertas")));

        if($_SESSION["id_user"] == "1"){
            $queuesAll = QueueManager_Manager::getQueueAll();
        }else{
            $queuesAll = QueueManager_Manager::getQueueAllPermission($_SESSION['id_user']);
        }

        $alerts = Alerts_Manager::getAll($this->connector);

        if(is_int($alerts) && $alerts != 204){
            $message = $this->view->translate("Error: Code ") . $alerts . $this->view->translate(". Please contact the administrator.");
            $this->_helper->redirector('sneperror','error','default',array('error_message'=>$message));

        }else{
            foreach ($queuesAll as $item => $queue) {
                if(is_array($alerts)):
                    foreach($alerts as $x => $alert){

                        if($queue['name'] == $alert->queue){

                            if(isset($alert->alerts->view->state)){
                                $queuesAll[$item]["view"] = (boolean)$alert->alerts->view->state;
                            }

                            if(isset($alert->alerts->sound->state)){
                                $queuesAll[$item]["sound"] = (boolean)$alert->alerts->sound->state;
                            }

                            if(isset($alert->alerts->mail->state)){
                                $queuesAll[$item]["mail"] = (boolean)$alert->alerts->mail->state;
                            }

                        }
                    }
                endif;
            }

            $this->view->queues = $queuesAll;
        }

    }

    /**
     * Config alerts
     */
    public function configAction(){

        $this->view->breadcrumb = Snep_Breadcrumb::renderPath(array(
                    $this->view->translate("Q-Manager"),
                    $this->view->translate("Configuração de Alertas"),
                    $this->view->translate("Edit")));

        $queue = $this->_request->getParam('name');
        $url = $this->connector."/?queue=".$queue;

        $alert = Alerts_Manager::getAlert($url);

        //Define the action and load form
        if($alert != 204){
            $this->view->alert = (array)$alert[0];
        }

        $this->renderScript( $this->getRequest()->getControllerName().'/config.phtml' );

        // After POST
        if ($this->_request->getPost()) {

            $dados = $this->_request->getParams();
            //if($dados["action_value"] == "add"){

                Alerts_Manager::remove($dados['name'], $this->connector);
                $httpcode = Alerts_Manager::add($dados, $this->connector);
            //}else{
            //    $httpcode = Alerts_Manager::edit($dados, $this->connector);
            //}

            $this->_redirect($this->getRequest()->getModuleName().'/'.$this->getRequest()->getControllerName());

        }

    }

}