<?php

/**
 * Q-Manager Dac Controller
 *
 * @category  Snep
 * @package   Q-Manager
 * @copyright Copyright (c) 2016 OpenS Tecnologia
 * @author    Tiago Zimmermann <tiago.zimmermann@opens.com.br>
 */
class SnepQManager_DacController extends Zend_Controller_Action {

	/**
     * Initial settings of the class
     */
    public function init() {

        $this->view->url = $this->getFrontController()->getBaseUrl() . '/' . $this->getRequest()->getModuleName() . '/' . $this->getRequest()->getControllerName();
        $this->view->lineNumber = Zend_Registry::get('config')->ambiente->linelimit;

        $this->connector = "http://127.0.0.1:3000/report/queue?";

        $this->view->baseUrl = Zend_Controller_Front::getInstance()->getBaseUrl();
        $this->view->key = Snep_Dashboard_Manager::getKey(
            Zend_Controller_Front::getInstance()->getRequest()->getModuleName(),
            Zend_Controller_Front::getInstance()->getRequest()->getControllerName(),
            Zend_Controller_Front::getInstance()->getRequest()->getActionName());
    }

    /**
     * indexAction
     */
    public function indexAction() {

        $this->view->breadcrumb = Snep_Breadcrumb::renderPath(array(
                    $this->view->translate("Q-Manager"),
                    $this->view->translate("Queue Report")));

        $config = Zend_Registry::get('config');

        // Get Queues and queue groups
        $this->view->groups = QueueManager_Manager::getGroupQueueAll();
        $queues = QueueManager_Manager::getQueueAll();

        if($_SESSION['id_user'] != '1'){
            $queues = QueueManager_Manager::getQueueAllPermission($_SESSION['id_user']);
        }

        $this->view->queues = $queues;
        $locale = Snep_Locale::getInstance()->getLocale();
        $this->view->datepicker_locale =  Snep_Locale::getDatePickerLocale($locale) ;

        $this->view->classRangeDate = "invisible";
        $this->view->classGroup = "invisible";
        $this->view->classSrc = "invisible";

        $this->view->translate("ringnoanswer");
        $this->view->translate("noanswer");
        $this->view->translate("abandon");
        $this->view->translate("enterqueue");
        $this->view->translate("answered");
        $this->view->translate("transfer");
        $this->view->translate("queuetimeout");
        $this->view->translate("queueabandon");

        if ($this->_request->getPost()) {

            $formData = $this->_request->getParams();
        	$this->viewAction();
        }

    }

    /**
     *  viewAction - View data in interface
     */
    public function viewAction(){

        $formData = $this->_request->getParams();
        //$line_limit = $this->view->lineNumber;
        $service_url = "";
        $allQueues = "";

        $dateFormat = QueueManager_Functions::FormatDateSelect($formData['period'],$formData['from'],$formData['to']);

        $param = "from=".$dateFormat['start_date']."&to=".$dateFormat['end_date'];

        //get options peer/agents if syntetic
        if($formData['type'] == 'analytic'){
            if($formData['src'] != ""){
                $param .= "&src=".trim($formData['src']);
            }
        }

        // get queues in group
        if($formData['option'] == 'group'){
            // limited in only queue for group
            $members = Snep_QueuesGroups_Manager::getMembers($formData['groups'][0]);
            foreach($members as $x => $member){
                $queues[$x]=  $member["id_queue"];
            }

            if($_SESSION['id_user'] != '1'){ // permission only queue
                $queuesPermission = QueueManager_Manager::getQueueAllPermission($_SESSION['id_user']);
                foreach($queuesPermission as $q => $item){
                    $queuesP[$q] = $item['id'];
                }
                $queues = array_intersect($queues, $queuesP);
            }

        }

        if($formData['option'] == 'queue'){
            $queues = $formData['queues'];
        }

        // request in array, as an API to receive a queue IN time
        $cont = 0;
        foreach($queues as $q => $id){
            $queue = QueueManager_Manager::getQueue($id);
            $req[$cont] = $param."&queue=".$queue['name'];
            $allQueues .= "#".$queue['name'].",";
            $cont++;
        }

        // queue Init for tab view
        $queueInit = explode("queue=", $req[0]);
        $this->view->queueInit = $queueInit[1];
        // end queueInit

        foreach($req as $i => $request){

            $service_url = $this->connector.$request;
            $service_url = str_replace(" ", "%20", $service_url);
            $queue = explode("queue=", $service_url);
            $http = curl_init($service_url);
            curl_setopt($http, CURLOPT_SSL_VERIFYPEER, false);
            $status = curl_getinfo($http, CURLINFO_HTTP_CODE);
            curl_setopt($http, CURLOPT_RETURNTRANSFER,1);
            $http_response = curl_exec($http);
            $httpcode = curl_getinfo($http, CURLINFO_HTTP_CODE);
            curl_close($http);

            switch ($httpcode) {
                case 200:
                    $response = json_decode($http_response);
                    if(isset($response->status) == 'Error'){
                        $message = $this->view->translate("Error: ") . $response->message;
                        $this->_helper->redirector('sneperror','error','default',array('error_message'=>$message));
                    }else{
                        $data[$queue[1]] = $response;
                    }
                    break;
                default:
                    $this->view->error = $this->view->translate("Error: Code ") . $httpcode . $this->view->translate(". Please contact the administrator.");
                    break;
            }
        }
        $queueinit = $queue[1];

        // form save
        $this->view->$formData['period'] = "selected";
        $this->view->$formData['type'] = "selected";
        $this->view->$formData['option'] = "selected";

        if($formData['period'] == "rangedate"){
            $this->view->from = $formData['from'];
            $this->view->to = $formData['to'];
            $this->view->classRangeDate = "visible";
        }

        $queues = QueueManager_Manager::getQueueAll();
        if($_SESSION['id_user'] != '1'){
            $queues = QueueManager_Manager::getQueueAllPermission($_SESSION['id_user']);
        }

        if($formData['option'] == "queue"){
            foreach ($formData['queues'] as $key => $value) {
                foreach ($queues as $x => $queue) {
                    if($value == $queue['id']){
                        $queues[$x]['selected'] = true;
                    }
                }
            }
            $this->view->classQueue = "visible";
            $this->view->classGroup = "invisible";
            $this->view->queues = $queues;
        }else{
            $groups = QueueManager_Manager::getGroupQueueAll();
            foreach ($formData['groups'] as $key => $value) {
                foreach ($groups as $x => $group) {
                    if($value == $group['id']){
                        $groups[$x]['selected'] = true;
                    }
                }
            }
            $this->view->classGroup = "visible";
            $this->view->classQueue = "invisible";
            $this->view->groups = $groups;
        }

        if($formData['type'] == 'analytic'){
            $this->view->src = $formData['src'];
            $this->view->classSrc = "visible";
        }
        // ends form save

        if(!isset($data)){
            $this->_helper->redirector('sneperror','error','default',array('error_message'=> "Erro ao gerar dados."));
        }

        $json = QueueManager_Format::formatter('dac',$formData['type'],$data);
        $data = json_decode($json);

        if(isset($formData['showcsv'])){

            $function =  "dacCsv".$formData['type'];
            $output = QueueManager_Functions::$function($data);
            $fileName = "DAC_".$formData['type'] .'_' .substr($dateFormat['start_date'], 0,10)."_".substr($dateFormat['end_date'], 0,10).".csv";

            header("Content-type: text/csv");
            header("Cache-Control: no-store, no-cache");
            header("Content-Disposition: attachment; filename=$fileName");

            echo $output;
            exit;

        }else{
            
            $this->view->data_init  = date("d/m/Y H:i:s", strtotime($dateFormat['start_date']));
            $this->view->data_end   = date("d/m/Y H:i:s", strtotime($dateFormat['end_date']));
            $this->view->data       = $data;
            $this->view->type       = $formData['type'];
            $this->view->$allQueues = substr($allQueues,0,-1);
            $this->renderScript('dac/index.phtml');
        }

    }

}
